def call(runOrNot) {
    pipeline {
        agent any
        triggers {
            cron('@daily')
        }
        stages {
            stage('A') {
                steps {
                    echo '========executing A========'
                }
                when {
                    expression {
                        script {
                            switch(runOrNot) {
                                case true:
                                    doRun = 'True'
                                    break
                                case false:
                                    doRun = 'False'
                                    break
                            }
                        }
                        sh (script: """python3 -c 'from test import *; test($doRun)'""", returnStdout: true)
                    }
                }
                post {
                    always {
                        echo '========always========1'
                    }
                    success {
                        echo '========A executed successfully========'
                    }
                    failure {
                        echo '========A execution failed========'
                    }
                }
            }
        }
        post {
            always {
                echo '========always========2'
            }
            success {
                echo '========pipeline executed successfully ========'
            }
            failure {
                echo '========pipeline execution failed========'
            }
        }
    }
}
